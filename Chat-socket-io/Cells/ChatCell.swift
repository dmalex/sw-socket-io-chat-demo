//
//  ChatCell.swift
//  SocketChat
//  B.RF Group, dmalex
//
import UIKit

class ChatCell: BaseCell
{
    @IBOutlet weak var lblChatMessage: UILabel!
    @IBOutlet weak var lblMessageDetails: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
