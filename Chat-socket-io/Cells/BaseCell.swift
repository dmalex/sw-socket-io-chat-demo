//
//  BaseCell.swift
//  SocketChat
//  B.RF Group, dmalex
//
import UIKit

class BaseCell: UITableViewCell
{
    override func awakeFromNib() {
        super.awakeFromNib()
        
        separatorInset = UIEdgeInsets.zero
        preservesSuperviewLayoutMargins = false
        layoutMargins = UIEdgeInsets.zero
        layoutIfNeeded()
        
        selectionStyle = UITableViewCell.SelectionStyle.none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
